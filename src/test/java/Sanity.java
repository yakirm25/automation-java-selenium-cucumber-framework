import baseTest.BaseTest;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import utils.Constants;
import utils.Listeners;

import static utils.report.ExtentReporter.logPass;

@ExtendWith(Listeners.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Sanity extends BaseTest {
    @Test
    public void test01_authentication() {
        logPass("Navigating to URL: " + Constants.URL);
        driver.get(Constants.URL);
        logPass("Clicking on the sign in button");
        homePage.clickSignInButton();
        logPass("Logging in with email: " + frameworkProperties.getProperty(Constants.EMAIL) + ", password: " + frameworkProperties.getProperty(Constants.PASSWORD));
        signInPage.login(frameworkProperties.getProperty(Constants.EMAIL), frameworkProperties.getProperty(Constants.PASSWORD));
        logPass("Verifying that the username is displayed correctly");
        verifications.verifyTextInElement(homePage.getUsername(), frameworkProperties.getProperty(Constants.USERNAME));
    }


    @Test
    public void test02_addingThingsToCart() {
        logPass("Navigating to URL: " + Constants.URL);
        driver.get(Constants.URL);
        logPass("Clicking on the shop button");
        homePage.clickShopButton();
        logPass("Adding an element to the cart");
        shopPage.addElementToCart();
        logPass("Verifying that the number of products in the cart is " + Constants.CART_QUANTITY);
        verifications.verifyNumberOfElementsFromText(shopPage.getNumberOfProducts(), Constants.CART_QUANTITY);
    }

    @Test
    public void test03_buyingProcess() {
        logPass( "Navigating to URL: " + Constants.URL);
        driver.get(Constants.URL);
        logPass( "Clicking on the shop button");
        homePage.clickShopButton();
        logPass("Adding an element to the cart");
        shopPage.addElementToCart();
        logPass("Proceeding to checkout");
        shopPage.proceedToCheckout();
        logPass("Proceeding to checkout on the cart page");
        cartPage.proceedToCheckout();
        logPass("Providing billing details");
        checkoutPage.provideBillingDetails();
        logPass("Placing the order");
        checkoutPage.placeOrder();
        logPass( "Verifying that the order status is " + Constants.ORDER_RECEIVED);
        verifications.verifyTextInElement(checkoutPage.getOrderStatus(), Constants.ORDER_RECEIVED);
    }


}



