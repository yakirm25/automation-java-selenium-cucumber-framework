package glue;

import config.AutomationFrameworkConfiguration;
import drivers.DriverSingleton;
import extensions.Verifications;
import io.cucumber.java.After;
import io.cucumber.java.BeforeAll;
import io.cucumber.spring.CucumberContextConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pageObjects.*;
import utils.*;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import utils.report.ExtentReporter;
import utils.report.ReportTestCases;

@CucumberContextConfiguration
@ContextConfiguration(classes = AutomationFrameworkConfiguration.class)
public class StepDefinitionBDD {
    private FrameworkProperties frameworkProperties;
    private WebDriver driver;
    private HomePage homePage;
    private SignInPage signInPage;
    private CheckoutPage checkoutPage;
    private ShopPage shopPage;
    private CartPage cartPage;
    private Verifications verifications;
    static ExtentReports report = new ExtentReports("report/TestReport.html");
    static ReportTestCases[] tests;
    private static final Logger logger = LogManager.getLogger(StepDefinitionBDD.class);



    @Autowired
    ConfigurationProperties configurationProperties;

    @BeforeAll
    public static void setupReport() {
        ExtentReporter.setup();
    }

    @Before
    public void initializeObjects(){
        frameworkProperties = new FrameworkProperties();
        DriverSingleton.getInstance(frameworkProperties.getProperty(Constants.BROWSER));
        homePage = new HomePage();
        signInPage = new SignInPage();
        checkoutPage = new CheckoutPage();
        shopPage = new ShopPage();
        cartPage = new CartPage();
        verifications = new Verifications();
        tests = ReportTestCases.values();
        //extentLogger = report.startTest(tests[Utils.testCount].getTestName());
        logger.info("Starting test: " + tests[Utils.testCount].getTestName());
        ExtentReporter.createTest(tests[Utils.testCount]);
        Utils.testCount++;
    }


    @Given("^I go to the Website")
    public void i_go_to_the_Website(){
        driver = DriverSingleton.getDriver();
        driver.get(Constants.URL);
        logger.info("INFO: Navigating to " + Constants.URL);
        ExtentReporter.logPass("Navigating to " + Constants.URL);
        //extentLogger.log(LogStatus.PASS, "Navigating to " + Constants.URL);
    }

    @When("^I click on Sign In button")
    public void i_click_on_sign_in_button(){
        homePage.clickSignInButton();
        ExtentReporter.logPass("Sign In button has been clicked.");
    }

    @And("^I add one element to the cart$")
    public void i_add_one_element_to_the_cart() {
        homePage.clickShopButton();
        shopPage.addElementToCart();
        ExtentReporter.logPass("One element were added to the cart");
    }

    @And("^I proceed to checkout")
    public void i_proceed_to_checkout(){
        shopPage.proceedToCheckout();
        cartPage.proceedToCheckout();
        ExtentReporter.logPass("We proceed to checkout");
    }

    @And("^I specify my credentials and click Login")
    public void i_specify_my_credentials_and_click_login(){
        signInPage.login(configurationProperties.getEmail(), configurationProperties.getPassword());
        ExtentReporter.logPass("Login has been clicked.");
    }
    @And("^I confirm address, shipping, payment and final order")
    public void i_confirm_address_shipping_payment_and_final_order(){
        checkoutPage.provideBillingDetails();
        checkoutPage.placeOrder();
        ExtentReporter.logPass("We confirm the final order");
    }

    @Then("^I can log into the website$")
    public void i_can_log_into_the_website(){
        verifications.verifyTextInElement(homePage.getUsername(), Constants.USERNAME);
        if(Constants.USERNAME.equals(homePage.getUsername().getText())) {
            ExtentReporter.logPass("The authentication is successful.");
        } else {
            ExtentReporter.logFail("Authentication is not successful.");
        }
    }

    @Then("^The elements are bought$")
    public void the_elements_are_bought(){
        String orderStatus = checkoutPage.getOrderStatus().getText();
        if(orderStatus.equals(Constants.ORDER_RECEIVED)) {
            ExtentReporter.logPass("The item was bought.");
        } else {
            ExtentReporter.logFail("The item was not bought.");
        }
        verifications.verifyTextInElement(checkoutPage.getOrderStatus(), Constants.ORDER_RECEIVED);
    }

    @After
    public void closeObjects(){
        logger.info(tests[Utils.testCount - 1].getTestName() + " has ended.");
        ExtentReporter.finalizeTest();
    }


}