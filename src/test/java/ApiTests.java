import api.ConstantsApi;
import api.FrameworkPropertiesApi;
import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.Matchers.*;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ApiTests {
    static FrameworkPropertiesApi properties;
    static String userId;

    @BeforeClass
    public static void prepareObjects() throws IOException {
        properties = new FrameworkPropertiesApi();
        testUserSignUp();
    }

    public static void testUserSignUp() throws IOException {
        Response response = null;
        String targetURI = ConstantsApi.BASE_URL + ConstantsApi.SIGN_UP_URL;

        JsonObject payload = new JsonObject();
        payload.addProperty(ConstantsApi.ID, properties.getPropValues(ConstantsApi.ID));
        payload.addProperty(ConstantsApi.USERNAME, properties.getPropValues(ConstantsApi.USERNAME));
        payload.addProperty(ConstantsApi.FIRST_NAME, properties.getPropValues(ConstantsApi.FIRST_NAME));
        payload.addProperty(ConstantsApi.LAST_NAME, properties.getPropValues(ConstantsApi.LAST_NAME));
        payload.addProperty(ConstantsApi.EMAIL, properties.getPropValues(ConstantsApi.EMAIL));
        payload.addProperty(ConstantsApi.COUNTRY_OF_RESIDENCE, properties.getPropValues(ConstantsApi.COUNTRY_OF_RESIDENCE));
        payload.addProperty(ConstantsApi.PHONE_NUMBER, properties.getPropValues(ConstantsApi.PHONE_NUMBER));
        payload.addProperty(ConstantsApi.ADDRESS, properties.getPropValues(ConstantsApi.ADDRESS));
        payload.addProperty(ConstantsApi.DESCRIPTION, properties.getPropValues(ConstantsApi.DESCRIPTION));
        payload.addProperty(ConstantsApi.PASSWORD, properties.getPropValues(ConstantsApi.PASSWORD));

        response = RestAssured.given()
                .relaxedHTTPSValidation()
                .header(ConstantsApi.CONTENT_TYPE, ConstantsApi.CONTENT_TYPE_VALUE)
                .body(payload)
                .when()
                .post(targetURI);

        assertEquals(ConstantsApi.SUCCESS_CODE, response.statusCode());
        JsonObject jsonResponse = response.as(JsonObject.class);
        userId = jsonResponse.get(ConstantsApi.ID).getAsString();
        System.out.println("UserID retrieved from the server: " + userId);
    }

    @Test
    public void testRetrieveAllUsers(){
        String targetURI = ConstantsApi.BASE_URL + ConstantsApi.RETRIEVE_URL;

        RestAssured.given()
                .relaxedHTTPSValidation()
                .header(ConstantsApi.CONTENT_TYPE, ConstantsApi.CONTENT_TYPE_VALUE)
                .when()
                .get(targetURI)
                .then()
                .statusCode(ConstantsApi.SUCCESS_CODE);
    }

    @Test
    public void testRetrieveSpecificUser(){
        String targetURI = ConstantsApi.BASE_URL + ConstantsApi.RETRIEVE_URL + ConstantsApi.SEPARATOR + userId;

        RestAssured.given()
                .relaxedHTTPSValidation()
                .header(ConstantsApi.CONTENT_TYPE, ConstantsApi.CONTENT_TYPE_VALUE)
                .when()
                .get(targetURI)
                .then()
                .statusCode(ConstantsApi.SUCCESS_CODE);
    }

    public static void deleteSpecificUser(){
        String targetURI = ConstantsApi.BASE_URL + ConstantsApi.RETRIEVE_URL + ConstantsApi.SEPARATOR + userId;

        RestAssured.given()
                .relaxedHTTPSValidation()
                .header(ConstantsApi.CONTENT_TYPE, ConstantsApi.CONTENT_TYPE_VALUE)
                .when()
                .delete(targetURI)
                .then()
                .statusCode(ConstantsApi.SUCCESS_CODE);
    }

    @AfterClass
    public static void cleanup(){
        deleteSpecificUser();
    }
}