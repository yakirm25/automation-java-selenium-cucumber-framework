package baseTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import drivers.DriverSingleton;
import extensions.Verifications;
import glue.StepDefinitionBDD;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import pageObjects.*;
import utils.*;
import utils.report.ExtentReporter;
import utils.report.ReportTestCases;
import utils.*;

public class BaseTest {
        protected static FrameworkProperties frameworkProperties;
        protected static WebDriver driver;
        protected static HomePage homePage;
        protected static SignInPage signInPage;
        protected static CheckoutPage checkoutPage;
        protected static ShopPage shopPage;
        protected static CartPage cartPage;
        protected static Verifications verifications;
       // protected static ExtentTest extentLogger;
        //protected static ExtentReports report = new ExtentReports("report/TestReport.html");
        protected static ReportTestCases[] tests;
        protected static final Logger logger = LogManager.getLogger(BaseTest.class);
        protected static ExtentReports extent;
        protected static ExtentTest test;

        @BeforeClass
        public static void initializeObjects(){
                frameworkProperties = new FrameworkProperties();
                DriverSingleton.getInstance(frameworkProperties.getProperty(Constants.BROWSER));
                driver = DriverSingleton.getDriver();
                homePage = new HomePage();
                signInPage = new SignInPage();
                checkoutPage = new CheckoutPage();
                shopPage = new ShopPage();
                cartPage = new CartPage();
                verifications = new Verifications();
                tests = ReportTestCases.values();
                ExtentReporter.setup();
                //extentLogger = report.startTest(tests[Utils.testCount].getTestName());
                logger.info("Starting test: " + tests[Utils.testCount].getTestName());

        }

        @Before
        public void createTest() {
                ExtentReporter.createTest(tests[Utils.testCount]);
                logger.info("Starting test: " + tests[Utils.testCount].getTestName());
                Utils.testCount++;
        }


        @After
        public void finalizeTest() {
            ExtentReporter.finalizeTest();
            logger.info("Finalizing test: " + tests[Utils.testCount - 1].getTestName());
        }
//
//        @AfterClass
//        public static void closeObjects() {
//                logger.info(tests[Utils.testCount - 1].getTestName() + " has ended.");
//                report.endTest(extentLogger);
//                driver.close();
//        }

        @AfterClass
        public static void tearDown() {
                logger.info(tests[Utils.testCount - 1].getTestName() + " has ended.");
                //logger.info("Tearing down BaseTest");
                //report.endTest(extentLogger);
                driver.close();
        }
}
