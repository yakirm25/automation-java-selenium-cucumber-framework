package pageObjects;

import drivers.DriverSingleton;
import extensions.UIActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import utils.Utils;


public class SignInPage {
    private WebDriver driver;

    public SignInPage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@name='xoo-el-username']")
    private WebElement signInEmail_txt;

    @FindBy(xpath = "//input[@name='xoo-el-password']")
    private WebElement password_txt;
    @FindBy(xpath = "//button[contains(text(),'Sign In')]")
    private WebElement signIn_btn;

    public void login(String email, String password) {
        UIActions.updateText(signInEmail_txt, email);
        UIActions.updateText(password_txt, Utils.decode64(password));
        UIActions.click(signIn_btn);
    }

}
