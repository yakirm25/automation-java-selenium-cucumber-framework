package pageObjects;

import drivers.DriverSingleton;
import extensions.UIActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class HomePage {
    private WebDriver driver;
    public HomePage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//ul[@id='menu-bluehost-website-builder']/li[1]")
    private WebElement homeButton;
    @FindBy(xpath = "//ul[@id='menu-bluehost-website-builder']/li[2]")
    private WebElement shopButton;

    @FindBy(xpath = "//ul[@id='menu-bluehost-website-builder']/li[5]")
    private WebElement signInButton;

    @FindBy(xpath = "//a[@href='https://bitheap.tech/my-account/']")
    private WebElement username;

    @FindBy(xpath = "(//a[contains(text(),'Logout')])[1]")
    private WebElement logout;

    public void clickHomeButton() {
        UIActions.click(homeButton);
    }
    public void clickSignInButton() {
        UIActions.click(signInButton);
    }

    public void clickShopButton() {
        UIActions.click(shopButton);
    }

    public WebElement getUsername() {
        return username;
    }

    public String getUsernameText() {
        return username.getText();
    }
}
