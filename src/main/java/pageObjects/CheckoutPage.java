package pageObjects;

import drivers.DriverSingleton;
import extensions.UIActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutPage {
    private WebDriver driver;

    public CheckoutPage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "billing_first_name")
    private WebElement firstName;

    @FindBy(id = "billing_last_name")
    private WebElement lastName;

    @FindBy(id = "billing_address_1")
    private WebElement address;

    @FindBy(id = "billing_postcode")
    private WebElement zipcode;

    @FindBy(xpath = "//input[@id='billing_city']")
    private WebElement townName;

    @FindBy(css = "#order_review > table > tfoot > tr.order-total > td > strong > span > bdi")
    private WebElement totalAmount;

    @FindBy(id = "place_order")
    private WebElement placeOrder;

    @FindBy(css = "#post-207 > header > h1")
    private WebElement orderStatus;

    @FindBy(xpath = "//input[@id='billing_email']")
    private WebElement emailAddress;


    @FindBy(xpath = "//input[@id='payment_method_cpmw']")
    private WebElement metaMaskPay;

    @FindBy(css = "button[class*='swal2-confirm']")
    private WebElement popupBTN;

    public void provideBillingDetails() {
        UIActions.updateText(firstName, "Yakir");
        UIActions.updateText(lastName, "Moshe");
        UIActions.updateText(address, "abc");
        UIActions.updateText(zipcode,"1055");
        UIActions.updateText(townName, "Chemin du Bas-de-Lavaux 17");
        UIActions.updateText(emailAddress,"Yakir@gmail.com");
        UIActions.clickRadioButton(metaMaskPay);
    }

    public String getTotalAmount() {
        return totalAmount.getText();
    }

    public void placeOrder() {
        UIActions.scrollPage(placeOrder);
        UIActions.clickJS(placeOrder);
        UIActions.clickJS(popupBTN);
    }

    public WebElement getOrderStatus(){
        return orderStatus;
    }

}
