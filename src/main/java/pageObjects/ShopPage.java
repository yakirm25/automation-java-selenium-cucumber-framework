package pageObjects;
import drivers.DriverSingleton;
import extensions.UIActions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Constants;
import utils.Utils;

public class ShopPage {
    private WebDriver driver;

    public ShopPage() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#main > ul > li.product.type-product.post-211.status-publish.instock.product_cat-uncategorized.purchasable.product-type-simple > a.button.wp-element-button.product_type_simple.add_to_cart_button.ajax_add_to_cart")
    private WebElement addToCartButton;

    @FindBy(css = "body > nav > div.wb4wp-wrapper > div.wb4wp-right > div > a > span")
    private WebElement numberOfProducts;

    @FindBy(css = "body > nav > div.wb4wp-wrapper > div.wb4wp-right > div > a")
    private WebElement cartButton;


    @FindBy(css = "[data-product_id='451']")
    private WebElement oneItem;

    public void addElementToCart() {
        UIActions.clickJS(oneItem);
        if(numberOfProducts.getText().contains(Constants.CART_QUANTITY))
            System.out.println("Cart has been updated");
        else {
            System.out.println("Cart has not been updated");
            //Utils.takeScreenshot();
        }
    }

    public String getNumberOfProductsText() {
        return numberOfProducts.getText();
    }
    public WebElement getNumberOfProducts() {
        return numberOfProducts;
    }

    public void proceedToCheckout(){
        UIActions.click(cartButton);
    }
}
