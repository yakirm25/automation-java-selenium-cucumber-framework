package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import drivers.DriverSingleton;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class Listeners implements TestWatcher {

    private WebDriver driver;
    private static final ExtentReports extent = new ExtentReports();
    private ExtentTest test;

    public Listeners() {
        driver = DriverSingleton.getDriver();
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        System.out.println("------------ Test: " + context.getDisplayName() + " Failed ------------");
        try {
            test.fail(cause.getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(takeScreenshot(driver)).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void testSuccessful(ExtensionContext context) {
        System.out.println("------------ Test: " + context.getDisplayName() + " Passed ------------");
    }

    @Override
    public void testAborted(ExtensionContext context, Throwable cause) {
        // TODO Auto-generated method stub
    }

    public void testStarted(ExtensionContext context) throws Exception {
        System.out.println("------------ Starting Test: " + context.getDisplayName() + " ------------");
        test = extent.createTest(context.getDisplayName());
    }

    @Override
    public void testDisabled(ExtensionContext context, Optional<String> reason) {
        System.out.println("------------ Test: " + context.getDisplayName() + " successfully Started ------------");
    }

    private String takeScreenshot(WebDriver driver) {
        String screenshotPath = "screenshots/" + System.currentTimeMillis() + ".png";
        try {
            TakesScreenshot ts = (TakesScreenshot) driver;
            File source = ts.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(source, new File(screenshotPath));
        } catch (IOException e) {
            System.out.println("Exception while taking screenshot " + e.getMessage());
        }
        return screenshotPath;
    }
}
