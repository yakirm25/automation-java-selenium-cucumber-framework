package utils;

public class Constants {
    public static final String PROP_FILE_NAME = "framework.properties";
    public static final String FILE_NOT_FOUND_EXCEPTION = "The Property file has not been found";

    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String PHANTOMJS = "PhantomJs";

    public static final int waitTime = 15;
    public static final String CART_QUANTITY = "1";
    public static final String COMPLETE_ORDER = "Your order on My Store is complete.";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String URL = "https://bitheap.tech";
    public static final String USERNAME = "Hello, yakir";
    public static final String BROWSER = "browser";
    public static final String ORDER_RECEIVED = "Order received";

    //ScreenShots
    public static final String SCREENSHOTS_FOLDER = "screenshots\\";
    public static final int SCREENSHOT_NAME_LENGTH = 4;
    public static final String SCREENSHOT_EXTENSION = ".png";
    public static final String FORMAT_DATE = "ddMMyyyy-HHmm";

}
