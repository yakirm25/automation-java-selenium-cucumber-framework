package utils.report;

import lombok.Getter;

@Getter
public enum ReportTestCases {
    T1("Testing the authentication"),
    T2("Testing the purchase of one item");

    private String testName;

    ReportTestCases(String testName) {
        this.testName = testName;
    }
}