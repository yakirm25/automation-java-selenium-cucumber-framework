package utils.report;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ExtentReporter {

    private static ExtentReports extent;
    private static ExtentTest test;

    public static void setup() {
        // create report folder if it doesn't exist
        File reportDir = new File("test-output");
        if (!reportDir.exists()) {
            reportDir.mkdir();
        }

        // create report file
        Date date = new Date();
        String reportFileName = "MyReport_" + date.toString().replace(":", "_").replace(" ", "_") + ".html";
        String reportFilePath = "test-output/" + reportFileName;
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportFilePath);

        // report configuration
        htmlReporter.config().setDocumentTitle("My Test Report");
        htmlReporter.config().setReportName("My Test Results");
        htmlReporter.config().setTheme(Theme.DARK);
        htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");

        // create ExtentReports object
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
    }

    public static void createTest(ReportTestCases testCase) {
        test = extent.createTest(testCase.getTestName());
    }

    public static void logStep(String stepName, Status status) {
        test.log(status, stepName);
    }

    public static void logInfo(String message) {
        test.info(message);
    }

    public static void logPass(String message) {
        Markup m = MarkupHelper.createLabel(message, ExtentColor.GREEN);
        test.pass(m);
    }

    public static void logFail(String message) {
        Markup m = MarkupHelper.createLabel(message, ExtentColor.RED);
        test.fail(m);
    }

    public static void logScreenshot(String imagePath) {
        try {
            test.addScreenCaptureFromPath(imagePath);
        } catch (IOException e) {
            logFail("Failed to attach screenshot");
        }
    }

    public static void finalizeTest() {
        extent.flush();
    }
}
