import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;

public class Log {
    private static Logger logger;

    public static Logger getLogger(Class<?> cls) {
        logger = LogManager.getLogger(cls);
        Configurator.initialize(null, "log4j2.xml");
        return logger;
    }
}
