package config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@ComponentScan("utils")
@PropertySources({
        @PropertySource("framework.properties")
})
public class AutomationFrameworkConfiguration {
    public AutomationFrameworkConfiguration(){}
}

