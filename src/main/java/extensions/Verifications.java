package extensions;
import static org.junit.jupiter.api.Assertions.assertAll;
import drivers.DriverSingleton;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;
import static org.junit.Assert.*;
import java.util.List;

public class Verifications {
    private static WebDriver driver;
    private static WebDriverWait wait;
    private static Actions action;
    private static JavascriptExecutor js;

    public Verifications() {
        driver = DriverSingleton.getDriver();
        wait = new WebDriverWait(driver, Long.parseLong(String.valueOf(Constants.waitTime)));
        action = new Actions(driver);
        js = (JavascriptExecutor) driver;
    }

    public void verifyTextInElement(WebElement elem, String expectedText) {
        wait.until(ExpectedConditions.visibilityOf(elem));
        String actualText = elem.getText().trim();
        assertEquals(expectedText, actualText);
    }

    public void verifyNumberOfElementsFromText(WebElement element, String expectedCount) {
        String actualCount = element.getText();
        assertEquals(Integer.parseInt(expectedCount), Integer.parseInt(actualCount));
    }


    public void verifyNumberOfElements(List<WebElement> elements, int expectedCount) {
        wait.until(ExpectedConditions.visibilityOf(elements.get(elements.size() - 1)));
        assertEquals(expectedCount, elements.size());
    }

    public void visibilityOfElements(List<WebElement> elems) {
        for (WebElement elem : elems) {
            assertTrue("sorry " + elem.getText() + " not displayed", elem.isDisplayed());
        }
        assertAll("Some elements were not displayed", () -> elems.forEach(elem -> assertTrue(elem.isDisplayed())));
    }


//    public static void visualElement(String expectedImageName) {
//        try {
//            screen.find(getData("ImageRepo") + expectedImageName + ".png");
//        } catch (FindFailed findFailed) {
//            System.out.println("Error Comparing Image File: " + findFailed);
//            fail("Error Comparing Image File: " + findFailed);
//        }
//    }


    public void existanceOfElement(List<WebElement> elements) {
        assertTrue(elements.size() > 0); //Checks if the list is empty or not
    }


    public void nonExistanceOfElement(List<WebElement> elements) {
        assertFalse(elements.size() > 0); //Checks if the list is empty or not
    }

    public void verifyText(String actual, String expected) {
        assertEquals(expected, actual);
    }

    public void verifyNumber(int actual, int expected) {
        assertEquals(expected, actual);
    }

    public void verifyClickSvgObject(WebElement elem) {
        assertTrue("Oh no, The Task is not Marked", elem.isDisplayed());
    }
}


