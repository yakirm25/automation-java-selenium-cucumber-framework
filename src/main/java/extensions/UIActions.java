package extensions;

import com.google.common.util.concurrent.Uninterruptibles;
import drivers.DriverSingleton;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;
import java.util.concurrent.TimeUnit;



public class UIActions {

    private static  WebDriver driver;
    private static  WebDriverWait wait;
    private static Actions action;
    private static JavascriptExecutor js;

    static {
        driver = DriverSingleton.getDriver();
        wait = new WebDriverWait(driver, Long.parseLong(String.valueOf(Constants.waitTime)));
        action = new Actions(driver);
        js = (JavascriptExecutor) driver;

    }
    private static void highlightElement(WebElement elem) {
        String originalStyle = elem.getAttribute("style");
        JavascriptExecutor js = (JavascriptExecutor) DriverSingleton.getDriver();
        js.executeScript("arguments[0].style.backgroundColor = 'yellow'", elem);
        Uninterruptibles.sleepUninterruptibly(500, TimeUnit.MILLISECONDS);
        js.executeScript("arguments[0].setAttribute('style', '" + originalStyle + "');", elem);
    }

    //@Step("Click on Element")
    public static void click(WebElement elem) {
        wait.until(ExpectedConditions.elementToBeClickable(elem));
        highlightElement(elem);
        elem.click();
    }

    public static void clickJS(WebElement elem) {
        wait.until(ExpectedConditions.elementToBeClickable(elem));
        highlightElement(elem);
        try {
            js.executeScript("arguments[0].click();", elem);
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void clickRadioButton(WebElement elem) {
        if (elem.isEnabled() && !elem.isSelected()) {
            highlightElement(elem);
            elem.click();
        }
    }

    public static void clickCheckBox(WebElement elem) {
        if (elem.isEnabled() && !elem.isSelected()) {
            elem.click();
        }
    }


    //@Step("Click on svgObject")
    public static void clickSvgObject(WebElement elem) {
        //  highlightElement(elem);
        action.click(elem).build().perform();
    }

    //@Step("Update Text Element")
    public static void updateText(WebElement elem, String text) {
        wait.until(ExpectedConditions.visibilityOf(elem));
        highlightElement(elem);
        clearText(elem);
        elem.sendKeys(text);
    }

    public static void clearText(WebElement elem) {
        elem.clear();
    }

    //@Step("Update Text Element as Human")
    public static void updateTextHuman(WebElement elem, String text) {
        wait.until(ExpectedConditions.visibilityOf(elem));
        highlightElement(elem);
        for (char ch : text.toCharArray()) {
            Uninterruptibles.sleepUninterruptibly(470, TimeUnit.MILLISECONDS);
            elem.sendKeys(ch + "");
        }
    }

    //@Step("Update DropDown Element By Index")
    public static void updateDropDownByIndex(WebElement elem, int index) {
        wait.until(ExpectedConditions.visibilityOf(elem));
        highlightElement(elem);
        Select dropDown = new Select(elem);
        dropDown.selectByIndex(index);
    }

    //@Step("Update DropDown Element By Text")
    public static void updateDropDownByText(WebElement elem, String text) {
        wait.until(ExpectedConditions.visibilityOf(elem));
        highlightElement(elem);
        Select dropDown = new Select(elem);
        dropDown.selectByVisibleText(text);
    }

    public static void clickOkInPopup(WebElement elem) {
        wait.until(ExpectedConditions.numberOfWindowsToBe(2));
        // Store the handle of the main window
        String mainWindowHandle = driver.getWindowHandle();

        // Switch to the popup window
        for (String popupHandle : driver.getWindowHandles()) {
            if (!popupHandle.equals(mainWindowHandle)) {
                driver.switchTo().window(popupHandle);
                break;
            }
        }
        elem.click();

        // Switch back to the main window
        driver.switchTo().window(mainWindowHandle);
    }

    //@Step("Mouse Hover Element")
    public static void mouseHover2(WebElement elem1, WebElement elem2) {
        highlightElement(elem1);
        action.moveToElement(elem1).moveToElement(elem2).click().build().perform();
    }

    //@Step("Mouse Hover Element")
    public static void mouseHover(WebElement elem1) {
        highlightElement(elem1);
        action.moveToElement(elem1).click().build().perform();
    }

    //@Step("Insert Key")
    public static void insertKey(WebElement elem, Keys value) {
        highlightElement(elem);
        elem.sendKeys(value);
    }

    //@Step("drag And Drop Element")
    public static void dragAndDrop(WebElement draggable, WebElement droppable) {
        action.dragAndDrop(draggable, droppable).build().perform();
    }


    //@Step("Scroll Down The Page")
    public static void scrollPage(WebElement elem) {
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("arguments[0].scrollIntoView(true);", elem);
    }
}
