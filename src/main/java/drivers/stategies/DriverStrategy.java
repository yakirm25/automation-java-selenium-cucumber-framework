package drivers.stategies;

import org.openqa.selenium.WebDriver;

public interface DriverStrategy {
    WebDriver setStrategy();
}
