package drivers.stategies;

import static utils.Constants.*;

public class DriverStrategyImplementer {

    public static DriverStrategy chooseStrategy(String strategy) {
        switch (strategy) {
            case CHROME:
                return new BrowserChrome();

            case PHANTOMJS:
                return new PhantomJs();

            case FIREFOX:
                return new BrowserFirefox();

            default:
                return null;
        }
    }
}
